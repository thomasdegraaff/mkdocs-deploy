# Mkdocs deploy

Clone the repository.
```
git clone https://gitlab.com/thomasdegraaff/mkdocs-deploy.git mkdocs-site
cd mkdocs-site
git remote rm origin
```
>Create a repository to store your mkdocs site.
```
git remote add [your-repo-url]
```

Initialize mkdocs site.
```
mkdocs new .
mkdocs serve
```
>Now create some site content.

Store and push content.
```
mkdocs build
git add .
git commit -m "Initialized site."
git push
```

Deploy site.
```
cp values.yaml.original values.yaml
vi values.yaml
helm install --create-namespace [the namespace from values.yaml] --kube-context [kubernetes context] --namespace [kubernetes namespace] .
```
